// import mongoose, { mongo } from 'mongoose';

import mongodb from 'mongodb';

export default async function mongo() {
  const password = 'kCnfCxIX5MEtWH23';
  const dbName = 'wb-parse-test';
  const client = await mongodb.connect(`mongodb+srv://admin:${password}@cluster0.ecjdj.mongodb.net/${dbName}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });
  return client;
}