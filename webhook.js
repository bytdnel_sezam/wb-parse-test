export default async function wildberriesAfterParseWebhook(url, success, failed) {
  try {
    const request = {
      message: "Parse succeeded",
      total: success.length + failed.length,
      success: {
        count: success.length,
        urls: success,
      },
      failed: {
        count: failed.length,
        urls: failed,
      }
    }
    const response = await axios.post(url, request, {timeout: 10000});
  } catch(reqErr) {
    console.log('Failed to request on webhook');  
  }
}