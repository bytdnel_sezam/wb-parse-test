import {
  getProductListPage,
  getProductPage,
  parseListPage,
  parseProductPage
} from './cheerio-parsing.js';

import { addQueueElement } from './queue.js';
import compression from 'compression';
import express from 'express';
import uuid from 'uuid';
import wildberriesAfterParseWebhook from './webhook.js';

export default function server() {
  const app = express()
  const port = process.env.PORT || 3000;
  
  app.use(express.json());
  app.use(compression());

  app.post('/parse/wb', async (req, res) => {
    const mongoClient = this.mongoClient;
    const {url, notificationURL} = req.body;
    const traceId = uuid();
    
    const parseListPage_ = parseListPage.bind({traceId});
    const getListPage_ = getProductListPage.bind({traceId});
    const parseProductPage_ = parseProductPage.bind({traceId});
    const getProductPage_ = getProductPage.bind({traceId}); 
    
    const urls = parseListPage_(await getListPage_(url));
    
    const queueElement = {
      ctx: {
        urls,
        traceId,
        notificationURL
      },
      work: async function() {
        const success = [];
        const failed = [];
        const products = [];
        for (const url of this.ctx.urls) {
          let html, product;
          try {
            html = await getProductPage_(url);
            product = parseProductPage_(html);
            console.log(product);
            products.push(product);
            success.push(url);
          } catch(productErr) {
            failed.push(url); 
          }
        }
        const result = await mongoClient
          .db('wb-parse-test')
          .collection('products')
          .insertMany(products);
        console.log({result});
        await wildberriesAfterParseWebhook(this.ctx.notificationURL, success, failed);
      }
    }

    addQueueElement(queueElement);

    res.status(200).json({
      traceId,
      parsedUrls: true,
      count: urls.length,
      notificationURL,
      date: Date.now(),
    })
  })
  
  app.listen(port, () => console.log('Listening to localhost:3000'));    
}
