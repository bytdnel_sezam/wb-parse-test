import mongo from './mongodb.js';
import queue from './cron.js';
import server from './server.js';

(async function() {
  const mongoClient = await mongo();
  server.apply({mongoClient});
  queue.apply({mongoClient});  
})()






