import axios from 'axios';
import cheerio from 'cheerio';

export async function getProductListPage(url) {
  console.log(this.traceId);
  
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (reqErr) {
    throw reqErr;
  }
}

export async function getProductPage(url) {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (reqErr) {
    throw reqErr;
  }
}

export function parseProductPage(html) {
  const $product = cheerio.load(html);
  
  let productInfo = {}
  
  $product("div[itemtype='http://schema.org/Product'] meta").each(function() {
    const el = $product(this);
    const key = el.attr('itemprop')
    const value = el.attr('content');
    productInfo[key] = value;
  });

  productInfo.vendorCode = $product('.article span').text();
  
  productInfo.images = [];
  $product('.swiper-wrapper img').each(function() {
    const $img = $product(this);
    productInfo.images.push({
      src: 'https:' + $img.attr('src').split('tm').join('big'),
      description: $img.attr('alt')
    });
  })
  
  const params = {};
  $product('.card-add-info .params .pp').each(function() {
    const param = $product(this);
    const key = param.children('span').first().text().trim();
    const value = param.children('span').last().text().trim();
    params[key] = value;
  });
  
  productInfo.params = params;
  
  return productInfo;
}

export function parseListPage(html) {
  console.log(this.traceId);
  const urls = []
  const $list = cheerio.load(html);
  $list('.j-card-item a').each(async function(i) {
    urls.push(`https://wildberries.ru${$list(this).attr('href')}`);
  });
  return urls;
}

