const queue = [];

export function getLastQueueElement() {
  console.log('get last element');
  console.log(queue);
  if (queue.length == 0) {
    return null;
  } else {
    return queue[queue.length-1];  
  }
}

export async function popQueueElement(element) {
  console.log('pop element');
  queue.splice(queue.indexOf(element));
  return true;
} 

export function addQueueElement(element) {
  console.log('add new queue element');
  queue.unshift(element);
  console.log(queue);
  return true;
}