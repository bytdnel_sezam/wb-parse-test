import { getLastQueueElement, popQueueElement } from './queue.js';

import { CronJob } from 'cron';

let queueIsWorking = false;

const workingInQueue = new CronJob('* * * * * *', async () => {
  console.log('cron start');
  if (!queueIsWorking) {
    console.log('queue is not working');
    const lastQueueElement = getLastQueueElement();
    if (!lastQueueElement) {
      console.log('queue is empty');
      workingInQueue.stop();
    }
    else {
      console.log('before starting work');
      queueIsWorking = true;
      await lastQueueElement.work();
      popQueueElement(lastQueueElement);
      queueIsWorking = false;
      console.log('after work');
    }
  }
})

const globalJob = new CronJob('0,10,20,30,40,50 * * * * *', () => {
  if (workingInQueue.running) {
    return;
  } else {
    workingInQueue.start();
  }
})

export default function queue() {
  console.log('Starting queue');
  globalJob.start();  
}
